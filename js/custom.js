// For Displaying Current Date And Time
var d=new Date();
var month = new Array();
month[0] = "January";
month[1] = "February";
month[2] = "March";
month[3] = "April";
month[4] = "May";
month[5] = "June";
month[6] = "July";
month[7] = "August";
month[8] = "September";
month[9] = "October";
month[10] = "November";
month[11] = "December";
var n = month[d.getMonth()];
document.getElementById("date").innerHTML=d.getDate()+" "+n+" "+d.getFullYear();
document.getElementById("time").innerHTML="&nbsp;"+d.getHours()+":"+d.getMinutes();
document.getElementById("year").innerHTML=d.getFullYear();

// For Slideshow Of Top Story 1
var i=0;
var path=new Array();
path[0]="images/_66381395_lfowe1r9.jpg";
path[1]="images/_80378853_80378378.jpg";
path[2]="images/_80354788_80354640.jpg";
function swapImage()
{
	document.getElementById("slide").src=path[i];
	if(i < path.length - 1) i++; else i = 0;
   setTimeout("swapImage()",3000);

}
swapImage();

// For Video Section Slideshow
document.getElementById('right_arrow').onclick=function(){
	var element=document.getElementsByClassName('slider_visible');
	for(var i=0;i<element.length;i++)
	{
		element[i].style.display="none";
	}
	var element=document.getElementsByClassName('slider_invisible');
	for(var i=0;i<element.length;i++)
	{
		element[i].style.display="block";
	}
	document.getElementById('left_arrow').style.opacity="1.0";
	document.getElementById('right_arrow').style.opacity="0.4";

};
document.getElementById('left_arrow').onclick=function(){
	var element=document.getElementsByClassName('slider_visible');
	for(var i=0;i<element.length;i++)
	{
		element[i].style.display="block";
	}
	var element=document.getElementsByClassName('slider_invisible');
	for(var i=0;i<element.length;i++)
	{
		element[i].style.display="none";
	}
	document.getElementById('left_arrow').style.opacity="0.4";
	document.getElementById('right_arrow').style.opacity="1.0";

};

// For Changing Content In Most Popular Section On Selecting Tabs Shared, Read And Video/audio
document.getElementById('shared').onclick=function()	{
	document.getElementById('shared_tab_open').style.display="block";
	document.getElementById('read_tab_open').style.display="none";
	document.getElementById('video_audio_tab_open').style.display="none";
	document.getElementById('shared_a').style.color="#D2700F";
	document.getElementById('read_a').style.color="#1F4F82";
	document.getElementById('video_a').style.color="#1F4F82";
	document.getElementById('shared').style.background="#FFF";
	document.getElementById('read').style.background="#EDEDED";
	document.getElementById('video_audio').style.background="#EDEDED";

};
document.getElementById('read').onclick=function()	{
	document.getElementById('shared_tab_open').style.display="none";
	document.getElementById('read_tab_open').style.display="block";
	document.getElementById('video_audio_tab_open').style.display="none";
	document.getElementById('shared_a').style.color="#1F4F82";
	document.getElementById('read_a').style.color="#D2700F";
	document.getElementById('video_a').style.color="#1F4F82";
	document.getElementById('read').style.background="#FFF";
	document.getElementById('shared').style.background="#EDEDED";
	document.getElementById('video_audio').style.background="#EDEDED";

};
document.getElementById('video_audio').onclick=function()	{
	document.getElementById('shared_tab_open').style.display="none";
	document.getElementById('read_tab_open').style.display="none";
	document.getElementById('video_audio_tab_open').style.display="block";
	document.getElementById('shared_a').style.color="#1F4F82";
	document.getElementById('read_a').style.color="#1F4F82";
	document.getElementById('video_a').style.color="#D2700F";
	document.getElementById('read').style.background="#EDEDED";
	document.getElementById('shared').style.background="#EDEDED";
	document.getElementById('video_audio').style.background="#FFF";

};